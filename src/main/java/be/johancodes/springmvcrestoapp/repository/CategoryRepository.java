package be.johancodes.springmvcrestoapp.repository;

import be.johancodes.springmvcrestoapp.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
}
