package be.johancodes.springmvcrestoapp.repository;

import be.johancodes.springmvcrestoapp.model.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface RestaurantRepository extends JpaRepository<Restaurant, Integer> {

    @Transactional
    @Query(value = "select r from Restaurant r where lower(r.restaurantName) like %:name% order by r.restaurantName asc ")
    List<Restaurant> searchRestaurantsByName(@Param("name") String name);
}
