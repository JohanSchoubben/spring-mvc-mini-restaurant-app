package be.johancodes.springmvcrestoapp.repository;

import be.johancodes.springmvcrestoapp.model.MenuItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface MenuItemRepository extends JpaRepository<MenuItem, Integer> {
    @Transactional
    @Query(value = "select i from MenuItem i where lower(i.itemName) like %:name% order by i.itemName asc")
    List<MenuItem> searchItemByName(@Param("name") String name);

    @Transactional
    @Query(value = "select i from MenuItem i where lower(i.category.description) like %:category% " +
            "group by i.category order by i.category.id asc")
    List<MenuItem> findAllByCategoryOrderByCategoryIdAsc(@Param("category") String category);
}
