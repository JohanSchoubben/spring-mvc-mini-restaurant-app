package be.johancodes.springmvcrestoapp.repository;

import be.johancodes.springmvcrestoapp.model.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends JpaRepository<City, Integer> {
}
