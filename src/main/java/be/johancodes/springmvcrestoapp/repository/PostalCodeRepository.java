package be.johancodes.springmvcrestoapp.repository;

import be.johancodes.springmvcrestoapp.model.PostalCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostalCodeRepository extends JpaRepository<PostalCode, Integer> {
}
