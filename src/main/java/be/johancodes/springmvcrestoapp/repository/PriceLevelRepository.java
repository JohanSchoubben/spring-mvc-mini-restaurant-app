package be.johancodes.springmvcrestoapp.repository;

import be.johancodes.springmvcrestoapp.model.PriceLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PriceLevelRepository extends JpaRepository<PriceLevel, Integer> {
}
