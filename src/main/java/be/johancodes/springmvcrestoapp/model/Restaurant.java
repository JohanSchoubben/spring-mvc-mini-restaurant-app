package be.johancodes.springmvcrestoapp.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Restaurant extends IdClass {

    @NotBlank(message = "Name cannot be left blank")
    private String restaurantName;
    @NotNull(message = "City is required")
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private City city;
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private PriceLevel priceLevel;
    private String photoUrl;
    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinTable(name = "restaurant_menuitems", joinColumns = @JoinColumn(name = "restaurant_id"),
            inverseJoinColumns = @JoinColumn(name = "menuitems_id"))
    private Set<MenuItem> menuItems = new HashSet<MenuItem>();

    public Restaurant addItemToRestaurant(MenuItem menuItem) {
        this.getMenuItems().add(menuItem);
        menuItem.getRestaurants().add(this);
        return this;
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "restaurantName='" + getRestaurantName() + '\'' +
                ", city=" + getCity().getCityName() +
                ", priceLevel=" + getPriceLevel().getPriceLevelName() +
                ", photoUrl='" + getPhotoUrl() + '\'' +
                ", menuItems=" + getMenuItems().stream().map(MenuItem::getItemName).collect(Collectors.toList()) +
                '}';
    }
}
