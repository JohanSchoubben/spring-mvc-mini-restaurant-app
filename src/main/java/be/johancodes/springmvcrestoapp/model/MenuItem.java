package be.johancodes.springmvcrestoapp.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MenuItem extends IdClass {

    @NotBlank(message = "Item name cannot be left blank")
    private String itemName;
    @NotNull(message = "Category is required")
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private Category category;
    @NotNull(message = "Price is required")
    private BigDecimal price;
    @NotBlank(message = "Photo url cannot be left blank")
    private String photoUrl;
    @ManyToMany(mappedBy = "menuItems", fetch = FetchType.EAGER)
    private Set<Restaurant> restaurants = new HashSet<Restaurant>();

    public MenuItem addRestaurantToItem(Restaurant restaurant) {
        restaurant.getMenuItems().add(this);
        this.getRestaurants().add(restaurant);
        return this;
    }

    @Override
    public String toString() {
        return "MenuItem{" +
                "itemName='" + getItemName() + '\'' +
                ", category=" + getCategory().toString() +
                ", price=" + getPrice() +
                ", photoUrl='" + getPhotoUrl() + '\'' +
                ", restaurants=" + getRestaurants().stream().map(Restaurant::getRestaurantName).collect(Collectors.toList()) +
                '}';
    }
}
