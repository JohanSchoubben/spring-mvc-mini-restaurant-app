package be.johancodes.springmvcrestoapp.model;

import lombok.*;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PostalCode extends IdClass {
    @NotBlank(message = "Postal code cannot be left blank")
    private String postalCode;
}
