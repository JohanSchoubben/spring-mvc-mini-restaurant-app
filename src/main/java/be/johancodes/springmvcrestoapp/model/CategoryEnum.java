package be.johancodes.springmvcrestoapp.model;

public enum CategoryEnum {
    STARTER("Voorgerecht"),
    SOUP("Soep"),
    MAIN_COURSE("Hoofdgerecht"),
    DESSERT("Dessert"),
    SNACK("Snack");

    private String categoryLabel;

    CategoryEnum(String categoryLabel) {
        this.categoryLabel = categoryLabel;
    }

    public String getCategoryLabel() {
        return categoryLabel;
    }
}
