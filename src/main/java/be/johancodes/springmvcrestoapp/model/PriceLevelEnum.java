package be.johancodes.springmvcrestoapp.model;

public enum PriceLevelEnum {
    BASIC(0.8),
    SPECIALIZED(1.0),
    TOP(1.25);

    private Double itemPriceFactor;

    PriceLevelEnum(Double itemPriceFactor) {
        this.itemPriceFactor = itemPriceFactor;
    }

    public Double getItemPriceFactor() {
        return itemPriceFactor;
    }
}
