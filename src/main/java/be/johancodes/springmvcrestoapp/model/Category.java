package be.johancodes.springmvcrestoapp.model;

import lombok.*;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Category extends IdClass {
    @NotBlank(message = "Category cannot be left blank")
    private String description;
}
