package be.johancodes.springmvcrestoapp.model;

import lombok.*;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PriceLevel extends IdClass {
    private String priceLevelName;
    private Double itemPriceFactor;
}
