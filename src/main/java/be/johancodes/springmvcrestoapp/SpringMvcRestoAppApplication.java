package be.johancodes.springmvcrestoapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvcRestoAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringMvcRestoAppApplication.class, args);
    }
}
