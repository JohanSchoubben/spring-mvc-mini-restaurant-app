package be.johancodes.springmvcrestoapp.bootstrap;


import be.johancodes.springmvcrestoapp.model.*;

import java.util.ArrayList;
import java.util.List;

public class BootstrapHelper {
    private static final String[] postalCodeData = {"1234", "2345", "3456", "4567", "4321"};
    private static final String[] cityNameData = {"Breukel", "Blazegem", "Kijksode", "Toenen"};
    private static final String[][] restaurantData = {
            {"Wendy's Eethoekje", "https://images.unsplash.com/photo-1495316364083-b5916626072e?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bmV1c2Nod2Fuc3RlaW4lMjBjYXN0bGV8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60"},
            {"In de Ark van Noah", "https://images.unsplash.com/photo-1474224348275-dd142b14f8c1?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTV8fG9sZCUyMHN0YXRpb258ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60"},
            {"Theo's Feels Like Home", "https://images.unsplash.com/photo-1574011444639-de77ca245a68?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mzh8fHNoYWNrfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60"},
            {"Take Your Pick Eatery", "https://images.unsplash.com/photo-1502137983922-433ff9e1b18f?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NjF8fHNoYWNrfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60"}
    };
    private static final String[][] itemData = {
            {"Luikse balletjes met aardappelpuree", "19.90", "https://images-5.schellywood.be/thumbnail/sidebar-wide/108083/balletjes-op-luikse-wijze.jpg"},
            {"Stoofvlees met gebakken appeltjes en frieten", "19.90", "https://images-4.schellywood.be/thumbnail/sidebar-wide/67470/knipsel.JPG"},
            {"Noordzeetong meunière met ovenaardappel", "25.60", "https://images-3.schellywood.be/thumbnail/sidebar-wide/12948/noordzeetong-meuniere-met-aardappelen-op-zout.png"},
            {"Vidé van vis met sauce hollandaise", "22.30", "https://images-4.schellywood.be/thumbnail/sidebar-wide/19095/visvidee-met-asperges.jpg"},
            {"Kervelsoep", "6.50", "https://images-3.schellywood.be/thumbnail/sidebar-wide/28903/kervelsoep.jpg"},
            {"Chinese tomatensoep", "5.50", "https://images-2.schellywood.be/thumbnail/sidebar-wide/88135/chinese-tomatensoep.jpg"},
            {"Asperges op Vlaamse wijze", "19.80", "https://images-2.schellywood.be/thumbnail/sidebar-wide/12170/asperges-op-vlaamse-wijze.png"},
            {"Tomaat met grijze garnalen", "21.30", "https://images-1.schellywood.be/thumbnail/sidebar-wide/14286/tomaat-garnaal.jpg"},
            {"Soesjes met vanille-ijs en warme chocoladesaus", "12.00", "https://images-3.schellywood.be/thumbnail/sidebar-wide/92887/soesjes-met-ijs.jpg"},
            {"Aardbeien met slagroom", "11.90", "https://images-2.schellywood.be/thumbnail/sidebar-wide/15004/meringue-met-aardbeien.jpg"}
    };

    public static List<PostalCode> generatePostalCodes() {
        List<PostalCode> postalCodes = new ArrayList<>();
        for (String data : postalCodeData) {
            postalCodes.add(new PostalCode(data));
        }
        return postalCodes;
    }

    public static List<String> generateCityNames() {
        List<String> cityNames = new ArrayList<>();
        for (String data : cityNameData) {
            cityNames.add(data);
        }
        return cityNames;
    }

    public static List<PriceLevel> generatePriceLevels() {
        List<PriceLevel> priceLevels = new ArrayList<>();
        PriceLevel newBasic = new PriceLevel();
        PriceLevelEnum basic = PriceLevelEnum.BASIC;
        newBasic.setPriceLevelName(basic.name());
        newBasic.setItemPriceFactor(basic.getItemPriceFactor());
        priceLevels.add(newBasic);
        PriceLevel newSpecialized = new PriceLevel();
        PriceLevelEnum specialized = PriceLevelEnum.SPECIALIZED;
        newSpecialized.setPriceLevelName(specialized.name());
        newSpecialized.setItemPriceFactor(specialized.getItemPriceFactor());
        priceLevels.add(newSpecialized);
        PriceLevel newTop = new PriceLevel();
        PriceLevelEnum top = PriceLevelEnum.TOP;
        newTop.setPriceLevelName(top.name());
        newTop.setItemPriceFactor(top.getItemPriceFactor());
        priceLevels.add(newTop);
        return priceLevels;
    }

    public static List<Category> generateCategories() {
        List<Category> categories = new ArrayList<>();
        for (CategoryEnum category : CategoryEnum.values()) {
            categories.add(new Category(category.getCategoryLabel()));
        }
        return categories;
    }

    public static String[][] getRestaurantData() {
        return restaurantData;
    }

    public static String[][] getItemData() {
        return itemData;
    }
}
