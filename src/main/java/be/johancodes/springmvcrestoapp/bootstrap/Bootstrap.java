package be.johancodes.springmvcrestoapp.bootstrap;

import be.johancodes.springmvcrestoapp.model.*;
import be.johancodes.springmvcrestoapp.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import static be.johancodes.springmvcrestoapp.bootstrap.BootstrapHelper.*;

@Component
@RequiredArgsConstructor
public class Bootstrap {
    private final RestaurantService restaurantService;
    private final MenuItemService menuItemService;
    private final PostalCodeService postalCodeService;
    private final CityService cityService;
    private final PriceLevelService priceLevelService;
    private final CategoryService categoryService;

    @PostConstruct
    public void init() {
        List<PostalCode> postalCodes = generatePostalCodes();
        postalCodeService.registerAll(postalCodes);

        List<String> cityNames = generateCityNames();
        List<City> cities = new ArrayList<>();
        for (int i = 0; i < cityNames.size(); i++) {
            City city = City.builder()
                    .postalCode(postalCodes.get(i))
                    .cityName(cityNames.get(i))
                    .build();
            cities.add(city);
        }
        cityService.registerAll(cities);

        List<PriceLevel> priceLevels = generatePriceLevels();
        priceLevelService.registerAll(priceLevels);

        List<Category> categories = generateCategories();
        categoryService.registerAll(categories);


        String[][] initialRestoData = getRestaurantData();
        for (int i = initialRestoData.length - 1; i >= 0; i--) {
            Restaurant restaurant = Restaurant.builder()
                    .restaurantName(initialRestoData[i][0])
                    .city(cities.get(i))
                    .priceLevel(priceLevels.get(2))
                    .photoUrl(initialRestoData[i][1])
                    .build();
            restaurantService.registerNewRestaurant(restaurant);
        }

        String[][] itemData = getItemData();
        for (String[] data : itemData) {
            MenuItem menuItem = MenuItem.builder()
                    .itemName(data[0])
                    .price(BigDecimal.valueOf(Double.parseDouble(data[1])))
                    .category(categories.get(0))
                    .photoUrl(data[2])
                    .build();
            menuItemService.registerMenuItem(menuItem);
        }

        for (int i = 0; i < initialRestoData.length; i++) {
            Restaurant restaurant = restaurantService.retrieveById(i + 1);
            MenuItem menuItem = menuItemService.retrieveById(i + 1);
            restaurant.addItemToRestaurant(menuItem);
            restaurantService.updateRestaurant(restaurant);
        }

        for (int i = 0; i < itemData.length; i++) {
            MenuItem menuItem = menuItemService.retrieveById(i + 1);
            if (i <= 3) {
                menuItem.setCategory(categories.get(2));
                menuItemService.updateMenuItem(menuItem);
            }
            if (i > 3 && i <= 5) {
                menuItem.setCategory((categories.get(1)));
                menuItemService.updateMenuItem(menuItem);
            }
            if (i > 5 && i <= 7) {
                menuItem.setCategory(categories.get(0));
                menuItemService.updateMenuItem(menuItem);
            }
            if (i > 7 && i <= 9) {
                menuItem.setCategory(categories.get(3));
                menuItemService.updateMenuItem(menuItem);
            }
            if (i > 9 && i <= 15) {
                menuItem.setCategory(categories.get(4));
                menuItemService.updateMenuItem(menuItem);
            }
        }
        Restaurant restaurant = restaurantService.retrieveById(4);
        restaurant.addItemToRestaurant(menuItemService.retrieveById(7));
        restaurant.addItemToRestaurant(menuItemService.retrieveById(10));
        restaurantService.updateRestaurant(restaurant);
        Restaurant restaurant1 = restaurantService.retrieveById(2);
        restaurant1.setPriceLevel(priceLevels.get(0));
        restaurantService.updateRestaurant(restaurant1);
        restaurant1.addItemToRestaurant(menuItemService.retrieveById(7));
        restaurant1.addItemToRestaurant(menuItemService.retrieveById(10));
        restaurantService.updateRestaurant(restaurant1);
        Restaurant ark = restaurantService.retrieveById(3);
        ark.setPriceLevel(priceLevels.get(1));
        restaurantService.updateRestaurant(ark);
        List<MenuItem> menuItemList = menuItemService.retrieveAllItems();
        // Filtering by Category
        System.out.println("Filtering by Category".toUpperCase(Locale.ROOT));

        menuItemList.stream().collect(Collectors.filtering((menuItem) ->
                        menuItem.getCategory().getDescription().contains("Hoofdgerecht"),
                Collectors.toList())).forEach(System.out::println);
        System.out.println();

        // Grouping by Category
        System.out.println("Grouping by Category".toUpperCase(Locale.ROOT));

        Map<Category, List<MenuItem>> itemsPerCategory =
                menuItemList.stream().collect(Collectors.groupingBy(MenuItem::getCategory));
        itemsPerCategory
                .forEach((category, menuItems) ->
                        System.out.println(category.getDescription() + " " + menuItems.stream().map(MenuItem::getItemName)
                                .collect(Collectors.toList())));
    }
}
