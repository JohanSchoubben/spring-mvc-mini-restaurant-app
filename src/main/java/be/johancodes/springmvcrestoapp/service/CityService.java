package be.johancodes.springmvcrestoapp.service;

import be.johancodes.springmvcrestoapp.model.City;

import java.util.List;

public interface CityService {
    City registerCity(City city);
    void removeCity(City city);
    List<City> registerAll(List<City> cities);
}
