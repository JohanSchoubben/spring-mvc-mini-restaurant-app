package be.johancodes.springmvcrestoapp.service;

import be.johancodes.springmvcrestoapp.model.PriceLevel;

import java.util.List;

public interface PriceLevelService {

    List<PriceLevel> registerAll(List<PriceLevel> priceLevels);
    void registerPriceLevel(PriceLevel priceLevel);
}
