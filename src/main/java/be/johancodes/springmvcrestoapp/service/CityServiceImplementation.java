package be.johancodes.springmvcrestoapp.service;

import be.johancodes.springmvcrestoapp.model.City;
import be.johancodes.springmvcrestoapp.repository.CityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CityServiceImplementation implements CityService {
    private final CityRepository cityRepository;
    @Override
    public City registerCity(City city) {
        return cityRepository.save(city);
    }

    @Override
    public void removeCity(City city) {
        cityRepository.delete(city);
    }

    @Override
    public List<City> registerAll(List<City> cities) {
        return cityRepository.saveAll(cities);
    }
}
