package be.johancodes.springmvcrestoapp.service;

import be.johancodes.springmvcrestoapp.model.Category;

import java.util.List;

public interface CategoryService {
    Category registerCategory(Category category);
    List<Category> registerAll(List<Category> categories);
    List<Category> retrieveAll();
    Category retrieveCategoryById(Integer id);
    void removeCategoryById(Integer id);
}
