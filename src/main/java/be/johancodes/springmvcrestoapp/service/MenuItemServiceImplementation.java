package be.johancodes.springmvcrestoapp.service;

import be.johancodes.springmvcrestoapp.model.MenuItem;
import be.johancodes.springmvcrestoapp.repository.MenuItemRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MenuItemServiceImplementation implements MenuItemService {
    private final MenuItemRepository menuItemRepository;

    @Override
    public List<MenuItem> registerAll(List<MenuItem> menuItemList) {
        return menuItemRepository.saveAll(menuItemList);
    }

    @Override
    public List<MenuItem> retrieveAllItems() {
        return menuItemRepository.findAll();
    }

    @Override
    public MenuItem retrieveById(Integer id) {
        return menuItemRepository.findById(id).orElse(null);
    }

    @Override
    public List<MenuItem> retrieveItemByNameLike(String name) {
        return menuItemRepository.searchItemByName(name);
    }

    @Override
    public MenuItem registerMenuItem(MenuItem menuItem) {
        return menuItemRepository.save(menuItem);
    }

    @Override
    public MenuItem updateMenuItem(MenuItem menuItem) {
        return menuItemRepository.save(menuItem);
    }
}
