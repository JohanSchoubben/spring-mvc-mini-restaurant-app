package be.johancodes.springmvcrestoapp.service;

import be.johancodes.springmvcrestoapp.model.Restaurant;

import java.util.List;

public interface RestaurantService {
    List<Restaurant> retrieveAll();
    List<Restaurant> registerAll(List<Restaurant> restaurantList);
    Restaurant retrieveById(Integer id);
    List<Restaurant> retrieveByNameSearch(String name);
    Restaurant registerNewRestaurant(Restaurant restaurant);
    Restaurant updateRestaurant(Restaurant restaurant);
}
