package be.johancodes.springmvcrestoapp.service;

import be.johancodes.springmvcrestoapp.model.Restaurant;
import be.johancodes.springmvcrestoapp.repository.RestaurantRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RestaurantServiceImplementation implements RestaurantService {
    private final RestaurantRepository restaurantRepository;

    @Override
    public List<Restaurant> retrieveAll() {
        return restaurantRepository.findAll();
    }

    @Override
    public List<Restaurant> registerAll(List<Restaurant> restaurantList) {
        return restaurantRepository.saveAll(restaurantList);
    }

    @Override
    public Restaurant retrieveById(Integer id) {
        return restaurantRepository.findById(id).orElse(null);
    }

    @Override
    public List<Restaurant> retrieveByNameSearch(String name) {
        return restaurantRepository.searchRestaurantsByName(name);
    }

    @Override
    public Restaurant registerNewRestaurant(Restaurant restaurant) {
        return restaurantRepository.save(restaurant);
    }

    @Override
    public Restaurant updateRestaurant(Restaurant restaurant) {
        return restaurantRepository.save(restaurant);
    }
}
