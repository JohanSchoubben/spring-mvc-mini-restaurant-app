package be.johancodes.springmvcrestoapp.service;

import be.johancodes.springmvcrestoapp.model.MenuItem;

import java.util.List;

public interface MenuItemService {
    List<MenuItem> registerAll(List<MenuItem> menuItemList);
    List<MenuItem> retrieveAllItems();
    MenuItem retrieveById(Integer id);
    List<MenuItem> retrieveItemByNameLike(String name);
    MenuItem registerMenuItem(MenuItem menuItem);
    MenuItem updateMenuItem(MenuItem menuItem);
}
