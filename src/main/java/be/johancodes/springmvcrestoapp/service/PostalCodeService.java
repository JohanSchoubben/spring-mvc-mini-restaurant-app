package be.johancodes.springmvcrestoapp.service;

import be.johancodes.springmvcrestoapp.model.PostalCode;

import java.util.List;

public interface PostalCodeService {
    PostalCode registerPostalCode(PostalCode postalCode);
    void removePostalCode(PostalCode postalCode);
    List<PostalCode> registerAll(List<PostalCode> postalCodes);
}
