package be.johancodes.springmvcrestoapp.service;

import be.johancodes.springmvcrestoapp.model.PostalCode;
import be.johancodes.springmvcrestoapp.repository.PostalCodeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PostalCodeServiceImplementation implements PostalCodeService {
    private final PostalCodeRepository postalCodeRepository;
    @Override
    public PostalCode registerPostalCode(PostalCode postalCode) {
        return postalCodeRepository.save(postalCode);
    }

    @Override
    public void removePostalCode(PostalCode postalCode) {
        postalCodeRepository.delete(postalCode);
    }

    @Override
    public List<PostalCode> registerAll(List<PostalCode> postalCodes) {
        return postalCodeRepository.saveAll(postalCodes);
    }
}
