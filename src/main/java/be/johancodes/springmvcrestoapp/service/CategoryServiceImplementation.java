package be.johancodes.springmvcrestoapp.service;

import be.johancodes.springmvcrestoapp.model.Category;
import be.johancodes.springmvcrestoapp.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryServiceImplementation implements CategoryService {

    private final CategoryRepository categoryRepository;

    @Override
    public Category registerCategory(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public List<Category> registerAll(List<Category> categories) {
        return categoryRepository.saveAll(categories);
    }

    @Override
    public List<Category> retrieveAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category retrieveCategoryById(Integer id) {
        return categoryRepository.findById(id).orElse(null);
    }

    @Override
    public void removeCategoryById(Integer id) {
        categoryRepository.deleteById(id);
    }
}
