package be.johancodes.springmvcrestoapp.service;

import be.johancodes.springmvcrestoapp.model.PriceLevel;
import be.johancodes.springmvcrestoapp.repository.PriceLevelRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PriceLevelServiceImplementation implements PriceLevelService {
    private final PriceLevelRepository priceLevelRepository;

    @Override
    public List<PriceLevel> registerAll(List<PriceLevel> priceLevels) {
        return priceLevelRepository.saveAll(priceLevels);
    }

    @Override
    public void registerPriceLevel(PriceLevel priceLevel) {
        priceLevelRepository.save(priceLevel);
    }
}
