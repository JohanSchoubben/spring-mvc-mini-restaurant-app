package be.johancodes.springmvcrestoapp.controller;

import be.johancodes.springmvcrestoapp.model.Category;
import be.johancodes.springmvcrestoapp.model.MenuItem;
import be.johancodes.springmvcrestoapp.service.MenuItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
public class MenuItemPageController {
    private final MenuItemService menuItemService;

    @GetMapping("/menuitems")
    public String showAllMenuItems(Model model) {
        model.addAttribute("menuitems", menuItemService.retrieveAllItems());
        return "/menuitems";
    }

    @GetMapping("/menuitem/{id}")
    public String showItemById(Model model, @PathVariable Integer id) { //TODO price is not calculated
        MenuItem menuItem = menuItemService.retrieveById(id);
        menuItem.getRestaurants().forEach((restaurant) ->
                menuItem.setPrice(menuItem.getPrice()
                        .multiply(BigDecimal.valueOf(restaurant.getPriceLevel().getItemPriceFactor()))
                        .setScale(2, RoundingMode.HALF_UP)));
        model.addAttribute("menuitem", menuItem);
        return "menuitemDetail";
    }

    @GetMapping(value = "/menuitems", params = "name")
    public String searchMenuItems(Model model, @RequestParam String name) {
        model.addAttribute("menuitems", menuItemService.retrieveItemByNameLike(name));
        return "/menuitems";
    }

    @GetMapping("/menuitem/new")
    public String showItemForm(Model model) {
        model.addAttribute("menuitem", new MenuItem());
        return "menuitemForm";
    }

    @PostMapping("/menuitem/new")
    public String addNewMenuItem(@Valid MenuItem menuItem, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "menuitemForm";
        } else {
            menuItemService.registerMenuItem(menuItem);
        }
        return "menuitemForm";
    }

    @GetMapping("/menuitem/{id}/edit")
    public String updateMenuItem(@PathVariable Integer id, Model model) {
        MenuItem menuItem = menuItemService.retrieveById(id);
        model.addAttribute("menuitem", menuItem);
        return "menuitemForm";
    }

    @GetMapping(value = "/menuitems-grouping")
    public String showAllItemsGroupingByCategory(Model model) {
        List<MenuItem> menuItemList = menuItemService.retrieveAllItems();
        Map<Category, List<MenuItem>> itemsPerCategory =
                menuItemList.stream().collect(Collectors.groupingBy(MenuItem::getCategory));
        model.addAttribute("itemspercategory", itemsPerCategory);
        return "/menuitemsgrouping";
    }
}
