package be.johancodes.springmvcrestoapp.controller;

import be.johancodes.springmvcrestoapp.model.Restaurant;
import be.johancodes.springmvcrestoapp.service.RestaurantService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Controller
@RequiredArgsConstructor
public class RestaurantPageController {
    private final RestaurantService restaurantService;

    @GetMapping("/restaurants")
    public String showAllRestaurants(Model model) {
        model.addAttribute("restaurants", restaurantService.retrieveAll());
        return "/restaurants";
    }

    @GetMapping("/restaurant/{id}")
    public String showRestaurantById(Model model, @PathVariable Integer id) {
        Restaurant restaurant = restaurantService.retrieveById(id);
        restaurant.getMenuItems().forEach((menuItem) ->
                menuItem.setPrice(
                        menuItem.getPrice()
                        .multiply(BigDecimal.valueOf(restaurant.getPriceLevel().getItemPriceFactor()))
                        .setScale(2, RoundingMode.HALF_UP)));
        model.addAttribute("restaurant", restaurant);
        return "restaurantDetail";
    }

    @GetMapping(value = "/restaurants", params = "name")
    public String searchRestaurants(Model model, @RequestParam String name) {
        model.addAttribute("restaurants", restaurantService.retrieveByNameSearch(name));
        return "/restaurants";
    }

    @GetMapping("/restaurant/new")
    public String showRestaurantForm(Model model) {
        model.addAttribute("restaurant", new Restaurant());
        return "restaurantForm";
    }

    @PostMapping("/restaurant/new")
    public String addNewRestaurant(@Valid Restaurant restaurant, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "restaurantForm";
        } else {
        restaurantService.registerNewRestaurant(restaurant);
        }
        return "restaurantForm";
    }

    @GetMapping("/restaurant/{id}/edit")
    public String editRestaurant(@PathVariable Integer id, Model model) {
        Restaurant restaurant = restaurantService.retrieveById(id);
        model.addAttribute("restaurant", restaurant);
        return "restaurantForm";
    }
}
